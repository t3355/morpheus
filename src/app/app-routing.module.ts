import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Firebase
// import { AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/compat/auth-guard'
// const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth/login']);

const routes: Routes = [
  // path: auth ==> Authorization
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule)},
  // path: step ==> Pages
  { path: 'step', loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)},
  // path: others ==> Others
  { path: 'others', loadChildren: () => import('./others/others.module').then( m => m.OthersModule)},
  { path: '', pathMatch: 'full', redirectTo: 'auth' },
  { path: '**', redirectTo: 'auth' }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot( routes )
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
