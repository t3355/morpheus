import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// Routing Module
import { AuthRoutingModule } from "./auth.routing";

// Components
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { RecoverComponent } from './recover/recover.component';

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        RecoverComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        AuthRoutingModule
    ]
})

export class AuthModule { }
