import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

// Components
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { RecoverComponent } from './recover/recover.component';

const routes: Routes = [
  {
    path: '', 
    children: [
        { path: 'login', component: LoginComponent},
        { path: 'register', component: RegisterComponent },
        { path: 'recover', component: RecoverComponent},
        { path: '**', redirectTo: 'login'}
    ]
  }
]

@NgModule ( {
    imports: [RouterModule.forChild(routes)]
    // exports: [RouterModule]
})

export class AuthRoutingModule {}
