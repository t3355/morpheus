import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

// Servicios
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  public loginForm = this.fb.group({
    eMail: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]]
  });

  constructor( private fb: UntypedFormBuilder,
               private userService: UsersService,
               private router: Router ) { }

  ngOnInit(): void {
    sessionStorage.removeItem('transaccion');    
  };

  async loginUser() {

    if ( this.loginForm.get('eMail')?.invalid ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'eMail',
        text: 'El correo debe ser un correo valido.'
      })
      .then( () => {
        console.log('Alerta de email cerrada.');
      });
    };

    if (this.loginForm.get('password')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Password',
        text: 'El password debe ser como minimo 6 caracteres.'
      });
    };

    if (this.loginForm.valid) {
      this.userService.loginUser( this.loginForm.value )
        .then( user => {
          if (!user?.emailVerified){
            Swal.fire({
              allowOutsideClick: false,
              icon: 'error',
              title: 'Verificacion de Email',
              text: `Debe validar su cuenta primero. Se envio a su correo ${user?.email}, un email de verificación.`
            })
          } else {
            // user?.getIdToken(false)
            //       .then( token => {
            //           sessionStorage.setItem( 'tokenId', token )
            //           console.log("TokenId => ", token);
            //       });
            const _uid = user?.uid || "";
            const _name = user?.displayName || "";
            sessionStorage.setItem( 'userUID', _uid );
            sessionStorage.setItem( 'userName', _name );
            this.router.navigateByUrl('step/one');
          };
        })
        .catch( () => {
          Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Error en el Login de la aplicación',
            text: 'El usuario o contraseña no son validas. Intente otra vez.'
          })
        });
    };
  };

}
