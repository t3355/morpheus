import { Component } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

// Service
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.css']
})
export class RecoverComponent {

  public registerForm = this.fb.group({
    eMail: ['', [Validators.required, Validators.email]]
  });

  constructor( private fb: UntypedFormBuilder, 
               private userService: UsersService,
               private router: Router ) { }

  async recoverUser() {
    let _email: string;

    if (this.registerForm.get('eMail')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'eMail',
        text: 'El correo debe ser un correo valido.'
      });
    };

    if ( this.registerForm.valid ) {
      _email = this.registerForm.get('eMail')?.value;
      console.log("Email : ", _email);

      await this.userService.recoverUser(_email)
                .then(() => {
                  // console.log("Se envio un email de recuperacion. : ");
                  Swal.fire({
                    allowOutsideClick: false,
                    icon: 'success',
                    title: 'Recuperar',
                    text: 'Se envio un correo de recuperacion de cuenta.'
                  }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                      this.router.navigateByUrl('auth/login');
                    };
                  });
                })
                .catch( error => { 
                  // console.log('Error Recover User : ', error);
                  Swal.fire({
                    allowOutsideClick: false,
                    icon: 'error',
                    title:'Recuperar',
                    text: 'La cuenta de correo no existe. Verifique que este bien escito.'
                  })
                });
    };
  };

}
