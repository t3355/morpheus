import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

// Service
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  public formSubmitted = false;

  public registerForm = this.fb.group({
    nameComplete: ['', [Validators.required, Validators.minLength(6)]],
    eMail: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPass: ['', [Validators.required, Validators.minLength(6)]]
  }, 
  {
    validators: this.passwordsEquals('password', 'confirmPass')
  });

  constructor( private fb: UntypedFormBuilder, 
               private userService: UsersService,
               private router: Router ) { }

  async createUser() {
    this.formSubmitted = true;
    // console.log( 'Formulario Registro => ', this.registerForm.value);

    if (this.registerForm.get('nameComplete')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Nombre',
        text: 'Debe de ingresar sus nombres y apellidos.'
      });
    };

    if (this.registerForm.get('eMail')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'eMail',
        text: 'El correo debe ser un correo valido.'
      });
    };

    if (this.registerForm.get('password')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Password',
        text: 'El password debe contener al menos 6 caracteres entre numero, letras y caracteres especiales.'
      });
    };

    if (this.registerForm.get('confirmPass')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Confirmar Password',
        text: 'El password de confirmación debe ser exactamente igual al password.'
      });
    };

    if ( this.registerForm.valid ) {
      
      await this.userService.createUser(this.registerForm.value)
        .then( userCreated => {

          const _UId = userCreated?.uid || "";
          // console.log("User UId : ", _UId);
          const _customerName = this.registerForm.get('nameComplete');
          
          this.userService.updateNameCustomer(_UId, _customerName?.value)
                .subscribe( user => {
                  console.log("User updateNameCustomer : ", user);
                  // return user;
                })
          Swal.fire({
            allowOutsideClick: false,
            icon: 'success',
            title: 'Usuario Creado',
            text: `Se envio a su correo ${userCreated?.email}, un email de verificacion de cuenta.`
            // confirmButtonText: 'Aceptar',
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              this.userService.logoutUser();
              sessionStorage.removeItem("tokenId");
              this.router.navigateByUrl('auth/login');
            }
          })
      })
      .catch( (error) => {
        console.log('Error register => ', error);
        Swal.fire({
          allowOutsideClick: false,
          icon: 'error',
          title: 'Error Creación de Usuario',
          text: 'Error en la creación del usuario'
        })
      });
    }
  }

  passwordsNoEquals() {
    const pass1 = this.registerForm.get('password')?.value;
    const pass2 = this.registerForm.get('confirmPass')?.value;

    if ( (pass1 === pass2) && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  passwordsEquals(pass1: string, pass2: string) {
    return ( formGroup: UntypedFormGroup) => {
      const password1 = formGroup.get(pass1);
      const password2 = formGroup.get(pass2);

      if ( password1?.value === password2?.value ) {
        password2?.setErrors(null)
      } else {
        password2?.setErrors({ noEsIgual: true })
      }
    }
  }

}
