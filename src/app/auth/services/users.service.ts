import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Firebase Auth, Functions
import { AngularFireAuth } from '@angular/fire/compat/auth';

// Variables Globales
import { environment } from 'src/environments/environment';

// Interface
import { LoginForm } from '../../interface/login-form.interface'

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( private http: HttpClient,
               private auth: AngularFireAuth
              ) { }

  async loginUser( loginForm: LoginForm ) {
    return await this.auth.signInWithEmailAndPassword(loginForm.eMail, loginForm.password)
                  .then( resp => {
                    let currentUser = resp.user;
                    // console.log( currentUser );
                    // return {"uid": currentUser?.uid, "tokenId": currentUser?.getIdToken()};
                    return currentUser;
                    // return currentUser?.getIdToken();
                  });
  };

  async logoutUser() {
    return await this.auth.signOut()
                  .catch( error => {
                    console.log( 'Error : ', error );
                  });
  };

  async createUser( loginForm: LoginForm ) {
    
    return await this.auth.createUserWithEmailAndPassword(loginForm.eMail, loginForm.password)
              .then(userCredential => {
                const currentUser = userCredential.user;
                // console.log("CurrentUser : ", currentUser);
                currentUser?.updateProfile({
                  displayName: loginForm.nameComplete
                });
                currentUser?.getIdToken().then(token => sessionStorage.setItem("tokenId", token));
                const configuracion = {
                  url: environment.apiAppWeb
                }
                currentUser?.sendEmailVerification(configuracion)
                      .catch( error => {
                        console.error("Error al enviar el correo de Verificacion: ", error);
                      });
                
                return currentUser
              })
              .then( userCreate => {
                // console.log('User Create : ', userCreate);
                // return userCreate?.getIdToken();
                return userCreate;
              });
  };

  updateNameCustomer(_UId: string, name: string) {
    console.log("_UId, service => ", _UId);
    console.log("Name, service => ", name);
    const _customer = {
      "CustomerName": name
    };
    return this.http.put(`${environment.apiURL}/api/customer/${_UId}`, _customer);
  };

  async recoverUser(eMail: string) {
    await this.auth.sendPasswordResetEmail( eMail );
  };

}
