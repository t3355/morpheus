import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http'

// Services
import { FunctionsService } from '../services/functions/functions.service';

// RXJS
import { Observable, throwError, from, lastValueFrom } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor( private functions: FunctionsService ) {}

  intercept(req: HttpRequest<any>, next:HttpHandler): Observable<HttpEvent<any>> {

    return from(this.handle(req, next));
  }

  async handle(req:HttpRequest<any>, next: HttpHandler) {

    let tokenId: string = "";
    await this.functions.getTokenId().then(token => tokenId = token);
    
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${tokenId}`
    });
    
    const reqClone = req.clone({
      headers
    });

    return await lastValueFrom(next.handle(reqClone)
                              .pipe(
                                catchError( this.manejarError )
                              ));
  };

  manejarError( error: HttpErrorResponse ) {
    
    let errorMessage = '';
    if ( error.status === 401 ) {
      errorMessage = error.message; 
    } else if ( error.status === 403 ) {
      console.log('Estado : ', error.message );
      errorMessage = error.message;
    } else {
      errorMessage = error.message;
    }

    // return throwError( errorMessage )
    return throwError( () => new Error(errorMessage) )
  };

}
