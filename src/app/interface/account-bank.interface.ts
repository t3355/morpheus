export interface AccountBank {
    Id?: string,
    AccountCurrency: string,
    AccountNumber: string,
    BankCode: string,
    BankName: string,
    Active: boolean
}
