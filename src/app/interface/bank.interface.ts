
export interface Bank {
    BankCode: string,
    BankName: string,
    CharacterNumber: number,
    Active: boolean
}