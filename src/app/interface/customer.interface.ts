export interface Customer {
    Id?: string,
    CustomerEMail: string,
    CustomerName: String,
    CustomerPhone: string,
    CustomerType: string,
    DocumentType: string,
    DocumentNumber: string,
    CreationDate?: number,
    ModifiedDate?: Date
}