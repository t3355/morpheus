export interface Exchange {
    BuyMoney: number,
    SaleMoney: number
}