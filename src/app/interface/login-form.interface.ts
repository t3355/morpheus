export interface LoginForm {
    nameComplete: string,
    eMail: string,
    password: string,
    confirmPass: string,
    phoneNumber: string
}