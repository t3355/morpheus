type AccountBank = {
    BankCode: string,
    BankName: string,
    BankAccount: string,
    Currency: string,
    Amount: number
};


export type Transaction = {
    CustomerName: string,
    CustomerUId: string,
    AccountSource: {
      BankCode: string,
      BankName: string,
      Currency: string,
      Amount: number
    },
    AccountTarget: AccountBank,
    ExchangeRateUsed: number,
    PathQR?: string,
    TransactionDate: number,
    TransactionNumber: string,
    TransactionStatus: string,
    TransferDate: number,
    TransferNumber: string,
    TransferSource: AccountBank,
    TransferTarget: AccountBank,
    TypeOperation: string
  };
