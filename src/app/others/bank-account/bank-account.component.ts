import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from "@angular/common";
import { UntypedFormBuilder, Validators } from "@angular/forms";

// Services
import { TransactionService } from '../../services/transaction.service';
import { FunctionsService } from '../../services/functions/functions.service';
import { CustomerService } from "../../services/customer/customer.service";

// Model
import { Bank } from '../../interface/bank.interface';
import { AccountBank } from "../../interface/account-bank.interface";

import Swal from 'sweetalert2';

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.component.html',
  styleUrls: ['./bank-account.component.css']
})
export class BankAccountComponent implements OnInit{

  bankNameSend = "";
  public bancos: Bank[] = [];

  public accountBank = this.fb.group({
    AccountCurrency: ['BO'],
    AccountNumber: ['', [Validators.required, Validators.minLength(10)]],
    Active: ['true', [Validators.required]],
    BankCode: [''],
    BankName: ['SEL', Validators.required],
    Id: [""]
  });

  customerAccount: AccountBank[] = [];

  @ViewChild('closebutton') closebutton: any;
  @ViewChild('closebuttonadd') closebuttonadd: any;

  constructor( private fb: UntypedFormBuilder,
               private functions: FunctionsService,
               private transactionService: TransactionService,
               private customerService: CustomerService,
               private location: Location ) {};

  ngOnInit(): void {
    // Valida el token, si el token caduco o esta vencido se realiza un Logout de la session
    // borra todas las variables de sesion y luego lo envia a la pantalla de logueo
    this.functions.validateToken();

    // Recupera la lista de Bancos.
    const Bancos = JSON.parse(sessionStorage.getItem("Banks") || "[]");
    if (Bancos.length > 0) {
      this.bancos = Bancos;
    } else {
      this.transactionService.Banks()
            .subscribe(_bancos => {
              sessionStorage.setItem("Banks", JSON.stringify(_bancos));
              this.bancos = _bancos;
            });
    };

    this.refreshData();
  };

  refreshData() {
    const UId = sessionStorage.getItem("userUID")!;
    // Llamado a la API que recupera las cuentas bancarias del cliente
    this.customerService.customerAccounts(UId)
        .then( ( _accounts ) => {
          this.customerAccount = _accounts;
          // console.log("Accounts Bank => ", this.customerAccount);
        });
  };

  insertAccount() {
    this.accountBank.patchValue({
      BankCode: "SEL",
      BankName: "Seleccione un Banco",
      AccountCurrency: "BO",
      AccountNumber: "",
      Active: true
    });
  };

  editAccount( account: AccountBank) {
    this.accountBank.patchValue({
          BankCode: account.BankCode,
          BankName: account.BankName,
          AccountCurrency: account.AccountCurrency,
          AccountNumber: account.AccountNumber,
          Active: account.Active,
          Id: account.Id
    });
    // console.log('FormBuilder => ', this.accountBank.value);
  };

  disableAccount(account: AccountBank) {
    if ( !account.Active ) {
      Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Inactiva',
        text: 'La Cuenta Bancaria ya esta inactiva.'
      });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        icon: 'question',
        title: 'Inactivar',
        text: `Desea inactivar la Cuenta ${account.AccountNumber}`,
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      })
      .then( (result) => {
        if (result.isConfirmed) {
          const UId = sessionStorage.getItem('userUID')!;
          const idAccountBank = account.Id!;
          this.customerService.disabledAccountBank(UId, idAccountBank)
              .subscribe( () => {
                Swal.fire({
                  allowOutsideClick: false,
                  icon: 'success',
                  title: 'Cuenta Inactiva',
                  text: `La Cuenta Bancaria: ${account.AccountNumber}, fue deshabilitada.`
                });
                this.refreshData();
              })
          //     .catch( error => {
          //       console.log( 'Error : ', error );
          //  });
          // console.log("Clic boton Disable. => ", account)
        };
      });
    };
  }

  async updateAccount() {
    const _accountBankCode = this.accountBank.get('BankCode')?.value;
    
    if ( _accountBankCode === "SEL" ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Bancaria',
        text: 'Debe de seleccionar un Banco.'
      });
    };

    if ( this.accountBank.get("AccountNumber")?.status === "INVALID" ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Bancaria',
        text: 'Debe ingresar un número de cuenta bancaria y no debe ser menos de 10 caracteres.'
      });
    };

    if ( this.accountBank.valid ) {
      const UId = sessionStorage.getItem('userUID')!;
      this.customerService.updateAccountBank(UId, this.accountBank.value)
            .subscribe((_acc: any) => {
              Swal.fire({
                allowOutsideClick: false,
                icon: 'success',
                title: 'Actualización Exitosa',
                text: 'La cuenta Bancaria fue actualizada exitosamente.'
              });
              this.refreshData();
            });
      this.closebutton.nativeElement.click();
    };
  };

  async saveAccount() {
    const _accountBankCode = this.accountBank.get('BankCode')?.value;
    
    if ( _accountBankCode === "SEL" ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Bancaria - Nuevo',
        text: 'Debe de seleccionar un Banco.'
      });
      return;
    };

    if ( this.accountBank.get("AccountNumber")?.status === "INVALID" ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Bancaria - Nuevo',
        text: 'Debe ingresar un número de cuenta bancaria y no debe ser menos de 10 caracteres.'
      });
    };

    if ( this.accountBank.valid ) {
      // console.log('Servicio Update => ', this.accountBank.value)
      const UId = sessionStorage.getItem('userUID')!;
      this.customerService.insertAccountBank(UId, this.accountBank.value)
            .subscribe((_acc: any) => {
              // console.log("Transaccion creada => ", _acc);
              Swal.fire({
                allowOutsideClick: false,
                icon: 'success',
                title: 'Creación Exitosa',
                text: 'La cuenta Bancaria fue creada exitosamente.'
              });
              this.refreshData();
            });
      this.closebuttonadd.nativeElement.click();
    };
  };

  selectBank( bankName: string) {
    this.accountBank.patchValue({
      BankName: bankName,
    });
  };

  cancelAccount() {
    this.location.back();
  };

}
