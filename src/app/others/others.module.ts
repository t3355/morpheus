import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

// Routing Module
import { OthersRoutingModule } from './others.routing';

// Module
import { UsualModule } from "../usual/usual.module";

// Components
import { OthersComponent } from './others.component';
import { ProfileComponent } from './profile/profile.component';
import { BankAccountComponent } from './bank-account/bank-account.component';

// Interceptors
import { InterceptorService } from '../interceptors/interceptor.service';

@NgModule({
  declarations: [
    OthersComponent,
    ProfileComponent,
    BankAccountComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    OthersRoutingModule,
    UsualModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass:InterceptorService,
      multi: true
    }
  ]
})
export class OthersModule { }
