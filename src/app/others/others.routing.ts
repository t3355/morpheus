import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { OthersComponent } from "./others.component";
import { ProfileComponent } from './profile/profile.component';

// Firebase
import { AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/compat/auth-guard'
import { BankAccountComponent } from './bank-account/bank-account.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth/login']);

const routes: Routes = [
  { 
    path: '', component:OthersComponent, 
    children: [
      {
        path: 'bankaccount', component: BankAccountComponent, pathMatch: 'full',
        canActivate: [AngularFireAuthGuard], data: {authGuardPipe: redirectUnauthorizedToLogin}
      },
      {
        path: 'profile', component: ProfileComponent, pathMatch: 'full',
        canActivate: [AngularFireAuthGuard], data: {authGuardPipe: redirectUnauthorizedToLogin}
      },
      { path: '', pathMatch: 'full', redirectTo: 'profile' },
      { path: '**', redirectTo: 'profile' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OthersRoutingModule { }
