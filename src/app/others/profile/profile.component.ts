import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { UntypedFormBuilder, Validators } from "@angular/forms";

import Swal from 'sweetalert2';

// Services
import { CustomerService } from '../../services/customer/customer.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public profileCustomer = this.fb.group({
    CustomerEMail: [''],
    CustomerName: ['', [Validators.required, Validators.minLength(5)]],
    CustomerPhone: ['', [Validators.minLength(8)]],
    CustomerType: ['SEL', Validators.required],
    DocumentType: ['SEL'],
    DocumentNumber: ['', [Validators.minLength(6)]]
  });

  IdCustomer: string = "";

  constructor( private fb: UntypedFormBuilder,
               private customernService: CustomerService,
               private location: Location ) { }

  ngOnInit(): void {
    // API que trae los datos del documento Usuario o Cliente
    this.IdCustomer = sessionStorage.getItem('userUID') || '';
    this.customernService.Customer(this.IdCustomer)
        .subscribe( customer => {
            this.profileCustomer.setValue({
              CustomerEMail: customer.CustomerEMail,
              CustomerName: customer.CustomerName,
              CustomerPhone: customer.CustomerPhone,
              CustomerType: customer.CustomerType,
              DocumentType: customer.DocumentType,
              DocumentNumber: customer.DocumentNumber
              // ModifiedDate: new Date()
            });
        })
  };

  selectType( customerType: string) {
    // this.bankNameSend = bankName;
  };

  selectTypeDocument(typeDocument: string){

  };

  async acceptProfile() {
    if (this.profileCustomer.get('CustomerName')?.invalid) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Perfil de Usuario',
        text: 'Debe completar su Nombre y Apellido'
      });
      return;
    };

    if (this.profileCustomer.get('CustomerType')?.value === 'SEL') {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Perfil de Usuario',
        text: 'Debe seleccionar un tipo de Personeria.'
      })
    }

    if (this.profileCustomer.valid) {
      this.customernService.updateCustomer(this.IdCustomer,this.profileCustomer.value)
              .subscribe( (result: any) => {
                if (result.Ok) {
                  Swal.fire({
                    allowOutsideClick: false,
                    icon: 'success',
                    title: 'Perfil de Usuario',
                    text: 'Datos guardados satisfactoriamente.'
                  })
                } else {
                  Swal.fire({
                    allowOutsideClick: false,
                    icon: 'error',
                    title: 'Perfil de Usuario',
                    text: 'Error al actualizar los datos del usuario. Error: ' + result.message
                  })                  
                }
      })      
    }
  };

  onCancelClick() {
    this.location.back();
  };
}
