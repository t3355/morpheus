import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeFourComponent } from './exchange-four.component';

describe('ExchangeFourComponent', () => {
  let component: ExchangeFourComponent;
  let fixture: ComponentFixture<ExchangeFourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExchangeFourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
