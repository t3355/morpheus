import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Service
import { TransactionService } from '../../services/transaction.service';
import { FunctionsService } from '../../services/functions/functions.service';

// Interface
import { Transaction } from '../../interface/transaction.interface';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-exchange-four',
  templateUrl: './exchange-four.component.html',
  styleUrls: ['./exchange-four.component.css']
})
export class ExchangeFourComponent implements OnInit, OnDestroy {

  transaction!: Transaction;
  procesing: Boolean = false;

  bankNameTarget:string = "";
  bankAccountTarget:string = "";
  bankCurrencyTarget:string = "";
  amountTarget: number = 0;
  typeOperation: string = "";
  transactionNumber = "";
  imageLoad: Boolean = false;

  constructor( private transactionService : TransactionService, 
               private router: Router,
               private functions: FunctionsService ) { }

  ngOnInit(): void {
    // Valida el token, si el token caduco o esta vencido se realiza un Logout de la session
    // borra todas las variables de sesion y luego lo envia a la pantalla de logueo
    this.functions.validateToken();
    
    // this.transaction = (window as {[key: string]:any})['transactionJSON'];

    if ( sessionStorage.getItem('transaccion') !== undefined ) {
      this.transaction = JSON.parse(sessionStorage.getItem('transaccion') || '{}');

      // this.transaction = (window as {[key: string]:any})['transactionJSON'];

      this.bankNameTarget = this.transaction.AccountTarget.BankName;
      this.bankAccountTarget = this.transaction.AccountTarget.BankAccount;
      this.bankCurrencyTarget = (this.transaction.AccountTarget.Currency === "DO") ? "Dolares Americanos" : "Bolivianos";
      this.amountTarget = this.transaction.AccountTarget.Amount;
      this.typeOperation = this.transaction.TypeOperation;
      this.transactionNumber = this.transaction.TransactionNumber;
      this.imageLoad = ((this.transaction.PathQR === "" || this.transaction.PathQR == null)? false : true);
    };
  };

  ngOnDestroy() {
    // (window as {[key: string]:any})['transactionJSON'] = {}
    sessionStorage.removeItem('transaccion');
  };

  saveTransaction() {
    this.procesing = true;
    this.transactionService.createTransaction(this.transaction)
          .subscribe((_tran: any) => {
            // console.log("Transaccion creada => ", _tran);
            this.procesing = true;
            Swal.fire({
              allowOutsideClick: false,
              icon: 'success',
              title: 'Transacción Exitosa',
              text: 'La solicitud de Compra/Venta fue enviada existosamente.'
            }).then( (result) => {
              if (result.isConfirmed) {
                this.router.navigateByUrl('/step/one')
              }
            });
          });
  };

}
