import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeOneComponent } from './exchange-one.component';

describe('ExchangeOneComponent', () => {
  let component: ExchangeOneComponent;
  let fixture: ComponentFixture<ExchangeOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExchangeOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
