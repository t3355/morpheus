import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

// Services
import { TransactionService } from '../../services/transaction.service';
import { FunctionsService } from '../../services/functions/functions.service';

// Model
import { Transaction } from '../../interface/transaction.interface';
import { Bank } from '../../interface/bank.interface';
import { Exchange } from '../../interface/exchange.interface';

@Component({
  selector: 'app-exchange-one',
  templateUrl: './exchange-one.component.html',
  styleUrls: ['./exchange-one.component.css']
})

export class ExchangeOneComponent implements OnInit, OnDestroy {

  transaction!: Transaction;

  // public buyMoney: number = 0;
  // public saleMoney: number = 0;
  public AmountMin: number = 1;
  public AmountMax: number = 0;
  
  public currencyTo = "Dolares";
  public currencyFrom = "Bolivianos";

  youSend: number = 100.00;
  sourceBank = "SEL";
  bankNameSend = "";
  youGet: number = 0;

  public bancos: Bank[] = [];

  public exchange: Exchange = { BuyMoney: 0.00, SaleMoney: 0.00 };
  
  constructor( private router: Router, 
               private transactionService: TransactionService,
               private functions: FunctionsService ) {
    // this._trans = (window as {[key: string]:any})['transactionJSON'];
    // console.log("_trans : ", this._trans);

    // Valida el token, si el token caduco o esta vencido se realiza un Logout de la session
    // borra todas las variables de sesion y luego lo envia a la pantalla de logueo
    this.functions.validateToken();
  }

  ngOnInit(): void {

    // Recupera la lista de Bancos.
    const Bancos = JSON.parse(sessionStorage.getItem("Banks") || "[]");
    if (Bancos.length > 0) {
      this.bancos = Bancos;
    } else {
      this.transactionService.Banks()
            .subscribe(_bancos => {
              const bankList = _bancos.filter(bank => bank.Active === true)
              sessionStorage.setItem("Banks", JSON.stringify(bankList));
              this.bancos = bankList;
            });
    };

    // Recupera el Tipo de Cambio
    this.moneyExchage()
        .finally( () => {
          // console.log('Conversion.')
          if (this.currencyTo == "Dolares") {
            this.youGet = (this.youSend * this.exchange.BuyMoney);
          } else {
            this.youGet = (this.youSend / this.exchange.SaleMoney);
          };
          // Redondea
          this.youGet = this.round(this.youGet);

          if ( sessionStorage.getItem('transaccion') === null ) {
            const _userUId = sessionStorage.getItem('userUID') || "";
            const _userName = sessionStorage.getItem('userName') || "";
            this.transaction = {
              CustomerName: _userName,
              CustomerUId: _userUId,
              TransactionStatus: "INPROCESS",
              TransactionDate: new Date().getTime(),
              TypeOperation:"Sale",
      
              ExchangeRateUsed: this.exchange.BuyMoney,
              TransactionNumber: "",
              TransferDate: new Date(0).getTime(),
              TransferNumber: "",
      
              AccountSource: {
                BankCode: this.sourceBank,
                BankName: this.bankNameSend,
                Currency: "DO",
                Amount: this.youSend
              },
              AccountTarget: {
                BankCode: "SEL",
                BankName: "",
                BankAccount: "",
                Currency: "BO",
                Amount: this.youGet
              },
              TransferSource: {
                BankCode: "",
                BankName: "",
                BankAccount: "",
                Currency: "DO",
                Amount: this.youSend
              },
              TransferTarget: {
                BankCode: "",
                BankName: "",
                BankAccount: "",
                Currency: "BO",
                Amount: this.youGet
              }
            };
          }
          else {
            this.transaction = JSON.parse(sessionStorage.getItem('transaccion') || '{}');
            this.youSend = this.transaction.AccountSource.Amount;
            this.youGet = this.transaction.AccountTarget.Amount;
            this.sourceBank = this.transaction.AccountSource.BankCode;
            this.bankNameSend = this.transaction.AccountSource.BankName;
            this.currencyTo = (this.transaction.AccountSource.Currency === "DO") ? "Dolares" : "Bolivianos";
            this.currencyFrom = (this.transaction.AccountSource.Currency === "DO") ? "Bolivianos" : "Dolares"
          };
        })
        
    // Recupera el valor maximo permitido.
    this.AmountMax = parseInt(sessionStorage.getItem("AmountMax")  || "0",10);
    if (this.AmountMax === 0 ) {
      this.transactionService.Parameters("ValueMax")
            .subscribe((_param: any) => {
              this.AmountMax = parseInt(_param["ParamValue"],10)
              sessionStorage.setItem("AmountMax",this.AmountMax.toString())
            });
    };
  };

  ngOnDestroy(): void {
    this.transaction.TypeOperation = (this.currencyTo == "Dolares") ? "Buy" : "Sale",
    this.transaction.ExchangeRateUsed = (this.currencyTo == "Dolares") ? this.exchange.BuyMoney : this.exchange.SaleMoney;
    this.transaction.AccountSource.BankCode = this.sourceBank;
    this.transaction.AccountSource.BankName = this.bankNameSend;
    this.transaction.AccountSource.Currency = (this.currencyTo == "Dolares") ? "DO" : "BO";
    this.transaction.AccountSource.Amount = this.youSend;
    this.transaction.AccountTarget.Currency = (this.currencyTo == "Dolares") ? "BO" : "DO";
    this.transaction.AccountTarget.Amount = this.youGet;

    this.transaction.TransferSource.Currency = this.transaction.AccountSource.Currency;
    this.transaction.TransferSource.Amount = this.transaction.AccountSource.Amount;
    this.transaction.TransferTarget.Currency = this.transaction.AccountTarget.Currency;
    this.transaction.TransferTarget.Amount = this.transaction.AccountTarget.Amount;
    
    // let value_tran;
    // (window as {[key: string]:any})['transactionJSON'] = this.transaction;
    // value_tran = (window as {[key: string]:any})['transactionJSON'];
    // console.log("_trans : ", value_tran);
    sessionStorage.setItem('transaccion', JSON.stringify(this.transaction));
  };

  async moneyExchage() {
    if ( sessionStorage.getItem('exchange') !== null ) {
      this.exchange = JSON.parse(sessionStorage.getItem('exchange')!);
      // this.buyMoney = this.exchange.Buy;
      // this.saleMoney = this.exchange.Sale;
    } else {
      await this.transactionService.ExchangeRate()
              .then( _tc => {
                // console.log('TC leyendo de la BD.');
                // console.log("TC => ", _tc);
                if ( _tc.length = 1 ) {
                  // this.buyMoney = _tc[0].BuyMoney;
                  // this.saleMoney = _tc[0].SaleMoney;
                  this.exchange = {
                    BuyMoney: _tc[0].BuyMoney,
                    SaleMoney: _tc[0].SaleMoney
                  };
                } else {
                  // this.buyMoney = 0;
                  // this.saleMoney = 0;
                  this.exchange = {
                    BuyMoney: 0.00,
                    SaleMoney: 0.00
                  };
                };
                sessionStorage.setItem('exchange',JSON.stringify(this.exchange));
              });
    };
  };

  changeCurrency() {
    if (this.currencyTo == "Dolares") {
      // Compra
      this.currencyTo = "Bolivianos";
      this.currencyFrom = "Dolares";
      this.youGet = (this.youSend / this.exchange.SaleMoney);
      // Trunca 
      // this.youGet = Number(this.youGet.toFixed(3).slice(0,-1)) 
    } else {
      // Venta
      this.currencyTo = "Dolares";
      this.currencyFrom = "Bolivianos";
      this.youGet = (this.youSend * this.exchange.BuyMoney);
    };
    // Redondea
    this.youGet = this.round(this.youGet);
  };

  round(num: number) {
    var m = Number((Math.abs(num) * 100).toPrecision(15));
    return Math.round(m) / 100 * Math.sign(num);
  };

  onSubmit( form: NgForm ) {

    if ( form.invalid ) { return; }

    let valueDollars: number = 0;
    
    if (this.currencyTo === "Dolares") {
      valueDollars = form.controls['youSend'].value;
    } else {
      valueDollars = form.controls['youGet'].value;
    };
    if (!( valueDollars >= this.AmountMin && valueDollars <= this.AmountMax)) {
      Swal.fire({
          allowOutsideClick: false,
          icon: 'error',
          title: 'Monto Excedido',
          text: `El valor de compra/venta de dolares americanos debe ser mayor igual a ${this.AmountMin} y menor igual a ${this.AmountMax}. El valor a adquirir es de : ${valueDollars} dolares americanos`
        });
      return;
    };

    if (form.controls['sourceBank'].value === "SEL") {
      Swal.fire({
          allowOutsideClick: false,
          icon: 'error',
          title: 'La Entidad Bancaria no fue seleccionada.',
          text: 'Debe seleccionar un banco.'
        });
      return;
    };

    this.router.navigateByUrl('/step/two');
  };

  calculateValueSend() {
    // console.log("Send");
    if (this.currencyTo == "Dolares") {
      this.youGet = (this.youSend * this.exchange.BuyMoney);
      this.youGet = this.round(this.youGet);
    } else {
      this.youGet = (this.youSend / this.exchange.SaleMoney);
      this.youGet = this.round(this.youGet); 
      // this.youGet = Number(this.youGet.toFixed(3).slice(0,-1));
    };
  };

  calculateValueGet() {
    // console.log("Get")
    if (this.currencyFrom == "Dolares") {
      this.youSend = (this.youGet * this.exchange.SaleMoney);
    } else {
      this.youSend = (this.youGet / this.exchange.BuyMoney);
    };
    this.youSend = this.round(this.youSend);
    // this.youSend = Number(this.youSend.toFixed(3).slice(0,-1));
  };

  sendValue( evento: KeyboardEvent  ) {
    const { target } = evento;
    
    const _valueSend = Number((target as HTMLButtonElement).value);
    this.calculateValueSend();
  };

  getValue( valueSend: string ) {
    const _valueSend = Number(valueSend);
    this.calculateValueGet();
  };

  selectBank( bankName: string) {
    this.bankNameSend = bankName;
  };

}
