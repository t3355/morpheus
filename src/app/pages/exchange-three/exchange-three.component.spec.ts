import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeThreeComponent } from './exchange-three.component';

describe('ExchangeThreeComponent', () => {
  let component: ExchangeThreeComponent;
  let fixture: ComponentFixture<ExchangeThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExchangeThreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
