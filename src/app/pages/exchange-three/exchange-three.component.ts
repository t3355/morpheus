import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms'

import Swal from 'sweetalert2';

// Firebase
import { AngularFireStorage } from '@angular/fire/compat/storage'
// import { getStorage, connectStorageEmulator } from 'firebase/storage';

// Interface
import { Transaction } from '../../interface/transaction.interface';

// Service
import { TransactionService } from '../../services/transaction.service';
import { FunctionsService } from '../../services/functions/functions.service';

// Rxjs
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-exchange-three',
  templateUrl: './exchange-three.component.html',
  styleUrls: ['./exchange-three.component.css']
})
export class ExchangeThreeComponent implements OnInit {

  transaction!: Transaction;
  transactionNumber: string = "";
  
  public accountCompany:string = "";
  public bankCodeCompany:string = "";
  public bankNameCompany:string = "";

  sourceAmount:number = 0;
  sourceCurrency_literal:string = "";
  sourceCurrency:string = "";
  imageURL!: Observable<string | null>;

  previousClick: Boolean = false;
  nextClick: Boolean = false;

  // accounts:any = [];
  typeOperation = "";

  constructor( private router: Router,
               private transactionService: TransactionService,
               private fireStorage: AngularFireStorage,
               private functions: FunctionsService 
               ) { }

  ngOnInit(): void {
    // const storage = getStorage()
    // connectStorageEmulator(storage, "localhost", 9199)
    
    // Valida el token, si el token caduco o esta vencido se realiza un Logout de la session
    // borra todas las variables de sesion y luego lo envia a la pantalla de logueo
    this.functions.validateToken();

    if ( sessionStorage.getItem('transaccion') !== undefined ) {
      this.transaction = JSON.parse(sessionStorage.getItem('transaccion') || '{}');

      // this.transaction = (window as {[key: string]:any})['transactionJSON'];

      this.transactionNumber = this.transaction.TransactionNumber;
      this.sourceAmount = this.transaction.AccountSource.Amount;
      this.sourceCurrency = this.transaction.AccountSource.Currency
      this.sourceCurrency_literal = (this.transaction.AccountSource.Currency === "DO") ? "Dolares Americanos" : "Bolivianos";
      this.typeOperation = this.transaction.TypeOperation;
    }

    this.transactionService.CompanyAccount(this.sourceCurrency)
              .pipe( map( (acc:any) => {
                // console.log('Cuenta de la Compania : ', acc);
                return acc;
              }))
              .subscribe( _accounts => {
                this.bankCodeCompany = _accounts[0].BankCode;
                this.bankNameCompany = _accounts[0].BankName;
                this.accountCompany = _accounts[0].AccountNumber;
                const _imageURL = _accounts[0].PathQR;
                // console.log("_imageURL : ", _imageURL);
                const ref = this.fireStorage.ref(_imageURL);
                
                this.imageURL = ref.getDownloadURL();
              });

    // this.transactionService.CompanyAccount(this.sourceCurrency)
    //   .then( (account) => {
    //     console.log("AccountCompany => ", account);
    //     this.bankCodeCompany = account[0].BankCode;
    //     this.bankNameCompany = account[0].BankName;
    //     this.accountCompany = account[0].AccountNumber;
    //   });
  };

  ngOnDestroy(): void {
    this.transaction.TransactionNumber = this.transactionNumber;
    this.transaction.TransferSource.BankAccount = this.accountCompany;
    this.transaction.TransferSource.BankName = this.bankNameCompany;
    this.transaction.TransferSource.BankCode = this.bankCodeCompany;
    this.transaction.TransferSource.Amount = this.sourceAmount;

    sessionStorage.setItem('transaccion', JSON.stringify(this.transaction));
    // (window as {[key: string]:any})['transactionJSON'] = this.transaction;
  };

  onSubmit( form: NgForm ) {

    if ( this.transactionNumber.length < 6) {
      // console.log("El número de transacción no debe ser menor a 6 caracteres.")
      Swal.fire({
          allowOutsideClick: false,
          icon: 'error',
          title: 'Número de caracteres',
          text: 'El número de transacción no debe ser menor de 6 digitos.'
        }
      );
      return;
    };

    if (this.previousClick) {
      this.router.navigateByUrl('/step/two');
    };
    if (this.nextClick) {
      this.router.navigateByUrl('/step/four');
    };
  };

  onPreviousClick() {
    this.previousClick = true;
    this.nextClick = false;
  };

  onNextClick() {
    this.nextClick = true;
    this.previousClick = false;
  };
}
