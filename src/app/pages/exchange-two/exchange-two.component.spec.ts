import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeTwoComponent } from './exchange-two.component';

describe('ExchangeTwoComponent', () => {
  let component: ExchangeTwoComponent;
  let fixture: ComponentFixture<ExchangeTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExchangeTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
