import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UntypedFormBuilder, Validators } from "@angular/forms";

// Firebase
import { Storage, ref, uploadBytes } from '@angular/fire/storage';
import { getStorage, connectStorageEmulator } from 'firebase/storage';

import Swal from 'sweetalert2';

// Services
import { TransactionService } from '../../services/transaction.service';
import { FunctionsService } from '../../services/functions/functions.service';
import { CustomerService } from 'src/app/services/customer/customer.service';

// Interface
import { Transaction } from '../../interface/transaction.interface';
import { Bank } from '../../interface/bank.interface';
import { AccountBank } from 'src/app/interface/account-bank.interface';

@Component({
  selector: 'app-exchange-two',
  templateUrl: './exchange-two.component.html',
  styleUrls: ['./exchange-two.component.css']
})
export class ExchangeTwoComponent implements OnInit, OnDestroy {

  transaction!: Transaction;

  targetBank:string = "SEL";
  bankNameTarget = "";
  targetAccount:string = "";
  userUId: string = "";
  public targetAmount:number = 0;

  public typeOperation = 'Buy';
  private previousClick: Boolean = false;
  private nextClick: Boolean = false;

  public bancos: Bank[]= [];
 
  imageLoad: Boolean = false;
  imageLoadError: Boolean = false;
  isNotImage: Boolean = true;
  imageLoading: Boolean = false;

  customerAccount: AccountBank[] = [];

  public accountBank = this.fb.group({
    AccountCurrency: [""],  //   "BO", 
    // AccountCurrency: [{value: "", disabled: true}],  //   "BO", 
    AccountNumber: ['', [Validators.required, Validators.minLength(10)]],
    Active: ['true', [Validators.required]],
    BankCode: [''],
    BankName: ['SEL', Validators.required],
    Id: [""]
  });

  @ViewChild('closebuttonadd') closebuttonadd: any;

  constructor( private router: Router,
               private fb: UntypedFormBuilder,
               private transactionService: TransactionService,
               private storage: Storage, 
               private functions: FunctionsService,
               private customerService: CustomerService ) {
    // Valida el token, si el token caduco o esta vencido se realiza un Logout de la session
    // borra todas las variables de sesion y luego lo envia a la pantalla de logueo
    this.functions.validateToken();
  }

  ngOnInit(): void {

    this.userUId = sessionStorage.getItem("userUID")!;
    
    const Bancos = JSON.parse(sessionStorage.getItem("Banks") || "[]");
    if (Bancos.length > 0) {
      this.bancos = Bancos;
    } else {
      console.log("Recupera los bancos del Servicio.")
      this.transactionService.Banks()
          .subscribe( _banks => {
            this.bancos = _banks;
          });
    };

    if ( sessionStorage.getItem('transaccion') !== undefined ) {
      this.transaction = JSON.parse(sessionStorage.getItem('transaccion') || '{}');

      // this.transaction = (window as {[key:string]:any})['transactionJSON'];

      this.targetBank = this.transaction.AccountTarget.BankCode;
      this.bankNameTarget = this.transaction.AccountTarget.BankName;
      this.targetAccount = this.transaction.AccountTarget.BankAccount;
      this.targetAmount = this.transaction.AccountTarget.Amount;

      this.typeOperation = this.transaction.TypeOperation;
    };

    this.loadCustomerAccount();

  };

  ngOnDestroy(): void {
    this.transaction.AccountTarget.BankCode = this.targetBank;
    this.transaction.AccountTarget.BankName = this.bankNameTarget;
    this.transaction.AccountTarget.BankAccount = this.targetAccount;

    // (window as {[key: string]:any})['transactionJSON'] = this.transaction;
    sessionStorage.setItem('transaccion', JSON.stringify(this.transaction));
  };

  onSubmit( form: NgForm ) {

    if (this.imageLoading) {
      return;
    }

    if (!this.imageLoad) {
      if ( form.invalid ) {
        Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Formulario incompleto',
            text: 'Debe seleccionar un Banco e ingresar el número de cuenta.'
          }
        );
        return;
      };
      
      if (form.controls['targetBank'].value === "SEL") {
        Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Banco no seleccionado',
            text: 'Debe seleccionar un banco.'
          }
        );
        return;
      };
  
      if ( this.targetAccount.length < 6) {
        Swal.fire({
            allowOutsideClick: false,
            icon: 'error',
            title: 'Número de caracteres',
            text: 'El número de caracteres de la cuenta no debe ser menor de 6 digitos.'
          }
        );
        return;
      };
    };

    if (this.previousClick) {
      this.router.navigateByUrl('/step/one');
    };
    if (this.nextClick) {
      this.router.navigateByUrl('/step/three');
    };
  };

  selectBank2( bankName: string) {
    this.accountBank.patchValue({
      BankName: bankName,
    });
  };

  onSubmit2( form: NgForm ) {

    if ( form.invalid ) {
      Swal.fire({
          allowOutsideClick: false,
          icon: 'error',
          title: 'Formulario incompleto',
          text: 'Debe seleccionar una Cuenta Bancaria.'
        }
      );
      return;
    };

    if (this.previousClick) {
      this.router.navigateByUrl('/step/one');
    };
    if (this.nextClick) {
      this.router.navigateByUrl('/step/three');
    };
  };

  selectBank( bankName: string) {
    this.bankNameTarget = bankName;
  };

  onPreviousClick() {
    this.previousClick = true;
    this.nextClick = false;
  };

  onNextClick() {
    this.nextClick = true;
    this.previousClick = false;
  };

  uploadQR($event: any) {
    const file:File = $event.target.files[0];
    const marcaTiempo = Date.now();
    const extension = file.type.substring(6)

    const nombreArchivo = `QR_${marcaTiempo.toString()}.${extension}`;

    // const imgRef = ref(this.storage, `users/${this.userUId}/${nombreArchivo}`);
    const imgRef = ref(this.storage, `users/${this.userUId}/${nombreArchivo}`);
    this.imageLoading = true;
    uploadBytes(imgRef, file)
      .then(_file => {
        this.imageLoad = true;
        this.transaction.PathQR = _file.metadata.fullPath;
        this.isNotImage = false;
        this.imageLoadError = false;
        this.imageLoading = false;
        // console.log(_file.metadata.fullPath)
      })
      .catch(error => {
        this.imageLoad = false;
        this.isNotImage = true;
        this.imageLoadError = true;
        console.log(error)
      });
  };

  addAccountBank() {
    this.accountBank.patchValue({
      BankCode: "SEL",
      BankName: "Seleccione un Banco",
      AccountCurrency: this.transaction.AccountTarget.Currency,
      AccountNumber: "",
      Active: true
    });
    this.accountBank.controls["AccountCurrency"].disable();
    this.accountBank.controls["Active"].disable();
  };

  selectAccount( texto: string) {
    const bank_Name = (texto.substring(texto.indexOf('-')+2, texto.lastIndexOf(':')-1)).trim()

    const objBank = this.bancos.find( (bank) => bank.BankName === bank_Name);
    if (objBank) {
      this.targetBank = objBank?.BankCode!;
      this.bankNameTarget = objBank?.BankName!;
    };
  };

  async saveAccount() {
    const _accountBankCode = this.accountBank.get('BankCode')?.value;
    
    if ( _accountBankCode === "SEL" ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Bancaria - Nuevo',
        text: 'Debe de seleccionar un Banco.'
      });
      return;
    };

    if ( this.accountBank.get("AccountNumber")?.status === "INVALID" ) {
      await Swal.fire({
        allowOutsideClick: false,
        icon: 'error',
        title: 'Cuenta Bancaria - Nuevo',
        text: 'Debe ingresar un número de cuenta bancaria y no debe ser menos de 10 caracteres.'
      });
    };

    if ( this.accountBank.valid ) {
      // console.log('Servicio Update => ', this.accountBank.value)
      const UId = sessionStorage.getItem('userUID')!;
      this.accountBank.controls["AccountCurrency"].enable();
      this.accountBank.controls["Active"].enable();
      
      this.customerService.insertAccountBank(UId, this.accountBank.value)
            .subscribe((_acc: any) => {
              
              this.loadCustomerAccount();

              Swal.fire({
                allowOutsideClick: false,
                icon: 'success',
                title: 'Creación Exitosa',
                text: 'La cuenta Bancaria fue creada exitosamente.'
              });
            });
      this.closebuttonadd.nativeElement.click();
    };
  };

  async loadCustomerAccount() {
    await this.customerService.customerAccountsCurrency(this.userUId, this.transaction.AccountTarget.Currency)
              .then( ( _accounts ) => {
                this.customerAccount = _accounts;
              });
  };

}
