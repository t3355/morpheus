import { Component, OnInit } from '@angular/core';

// Environment
import { environment } from 'src/environments/environment';

// Firebase
import { getStorage, connectStorageEmulator } from '@angular/fire/storage';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit{

  constructor( ) { }

  ngOnInit(): void {
    if ( environment.useEmulators) {
      const storage = getStorage()
      connectStorageEmulator(storage, "localhost", 9199)
  };
  }

}
