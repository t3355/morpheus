import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Routing Module
import { PagesRoutingModule } from './pages.routing';

// Module
import { UsualModule } from "../usual/usual.module";

// Components
import { PagesComponent } from './pages.component';
import { ExchangeOneComponent } from './exchange-one/exchange-one.component';
import { ExchangeTwoComponent } from './exchange-two/exchange-two.component';
import { ExchangeThreeComponent } from './exchange-three/exchange-three.component';
import { ExchangeFourComponent } from './exchange-four/exchange-four.component';

// Interceptors
import { InterceptorService } from '../interceptors/interceptor.service';

@NgModule({
  declarations: [
    PagesComponent,
    ExchangeOneComponent,
    ExchangeTwoComponent,
    ExchangeThreeComponent,
    ExchangeFourComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    UsualModule,
    PagesRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ]
})
export class PagesModule { }
