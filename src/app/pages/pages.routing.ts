import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { ExchangeFourComponent } from './exchange-four/exchange-four.component';
import { ExchangeOneComponent } from './exchange-one/exchange-one.component';
import { ExchangeThreeComponent } from './exchange-three/exchange-three.component';
import { ExchangeTwoComponent } from './exchange-two/exchange-two.component';
import { PagesComponent } from './pages.component';

// Firebase
import { AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/compat/auth-guard'

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth/login']);

const routes: Routes = [
  { 
    path:'', component:PagesComponent,
    children: [
        { path: 'one', component: ExchangeOneComponent,  pathMatch: 'full', 
          canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } 
        },
        { path: 'two', component: ExchangeTwoComponent,  pathMatch: 'full', 
          canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } 
        },
        { path: 'three', component: ExchangeThreeComponent,  pathMatch: 'full', 
          canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } 
        },
        { path: 'four', component: ExchangeFourComponent,  pathMatch: 'full', 
          canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } 
        },
        { path: '', pathMatch: 'full', redirectTo: 'one' },
        { path: '**', pathMatch: 'full', redirectTo: 'one' },
    ],
  },
]

@NgModule({
    imports: [ RouterModule.forChild( routes )],
    exports: [ RouterModule ]
})
export class PagesRoutingModule { }
