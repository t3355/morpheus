import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Interface
import { AccountBank } from "../../interface/account-bank.interface";
import { Customer } from '../../interface/customer.interface';

// Rxjs
import { firstValueFrom } from 'rxjs';

// Enviroment
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor( private http: HttpClient ) { }

  // getCustomerAccount() {
  //   return firstValueFrom(this.http.get<any>('assets/data/account-bank.json'))
  //       .then( res => res.data as AccountBank[] )
  //       .then( data => data );
  // };

  Customer( _customerId: string ) {
    return this.http.get<Customer>(`${environment.apiURL}/api/customer/${_customerId}`);
  };

  updateCustomer( idCustomer: string, customer: Customer) {
    return this.http.put(`${environment.apiURL}/api/customer/${idCustomer}`, customer);
  };

  customerAccounts(IdCustomer: string) {
    return firstValueFrom(this.http.get<AccountBank[]>(`${environment.apiURL}/api/customeraccount/${IdCustomer}`))
  };

  async customerAccountsCurrency(IdCustomer: string, IdCurrency: string) {
    return await firstValueFrom(this.http.get<AccountBank[]>(`${environment.apiURL}/api/customeraccount/${IdCustomer}/${IdCurrency}`))
  };

  insertAccountBank( IdCustomer: string, account: AccountBank) {
    return this.http.post(`${environment.apiURL}/api/customeraccount/${IdCustomer}`, account);
  };

  updateAccountBank( IdCustomer: string, account: AccountBank ) {
    // console.log('Actualiza Cuenta Bancaria => ', account);
    return this.http.put(`${environment.apiURL}/api/customeraccount/${IdCustomer}`, account);
  };

  disabledAccountBank( IdCustomer: string, IdAccountBank: string) {
    return this.http.delete(`${environment.apiURL}/api/customeraccount/${IdCustomer}/${IdAccountBank}`)
  };

}
