import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// Firebase
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireFunctions } from '@angular/fire/compat/functions';

// Services
import { UsersService } from '../../auth/services/users.service';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor( private router: Router,
               private userService: UsersService,
               private functions : AngularFireFunctions,
               private auth: AngularFireAuth ) { }

  async validateToken() {
    let tokenId: string = "";
    await this.getTokenId().then(token => tokenId = token);
    // console.log("Valida tokenId => ", tokenId);

    if (tokenId !== '') {
      this.verifiedToken(tokenId)
            .then( result => {
              if ( !result.Ok ) {
                  this.userService.logoutUser()
                      .then( () => {
                        // sessionStorage.removeItem('tokenId');
                        sessionStorage.removeItem('userUID');
                        sessionStorage.removeItem('userName');
                        sessionStorage.removeItem('transaccion');
                        sessionStorage.removeItem('Banks');
                        sessionStorage.removeItem('AmountMax');
                        this.router.navigateByUrl('auth/login');
                      })
                      .catch( error => {
                        console.log( 'Error : ', error );
                      });
              };
            })
    } else {
      this.userService.logoutUser();
      sessionStorage.removeItem('userUID');
      sessionStorage.removeItem('userName');
      sessionStorage.removeItem('transaccion');
      sessionStorage.removeItem('Banks');
      sessionStorage.removeItem('AmountMax');
      this.router.navigateByUrl('auth/login');
    };
  };

  async getTokenId(): Promise<string> {
    return await this.auth.currentUser
     .then ( user => user?.getIdToken(false)
      .then( token => {
        return token;
      })
    ) || "";
  };

  verifiedToken( tokenId: string) {
    const data = {
      tokenId: tokenId
    }
    const _verifiedToken = this.functions.httpsCallable('verifyTokenId');
    return _verifiedToken( data ).toPromise();
          // .then( (result) => { return result;})
          // .catch( (errorCatch) => { console.log('error Catch serviceTransation', errorCatch ); }) 
  };

}
