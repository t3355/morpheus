import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Firebase Functions
import { AngularFireFunctions } from '@angular/fire/compat/functions';

// Interfaces
import { Transaction } from '../interface/transaction.interface';
import { Bank } from '../interface/bank.interface';
import { Exchange } from '../interface/exchange.interface';

// Varables Globales de Entorno
import { environment } from 'src/environments/environment';

// Rxjs
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor( private functions : AngularFireFunctions,
               private http: HttpClient ) { }

  createTransaction( transaction: Transaction ) {
    return this.http.post(`${environment.apiURL}/api/transaction`, transaction);
  };

  CompanyAccount( currency: string) {
    // const data = {
    //   Currency: currency
    // }
    
    // const _account = this.functions.httpsCallable('getCompanyAccount');
    // return _account( { Currency: currency } ).toPromise()
    // console.log("Moneda : ", currency);
    return this.http.get(`${environment.apiURL}/api/account/${currency}`);
  };

  Banks() {
    return this.http.get<Bank[]>(`${environment.apiURL}/api/bank`);
  };

  ExchangeRate () {
    return firstValueFrom(this.http.get<Exchange[]>(`${environment.apiURL}/api/exchangerate`));
  };

  Parameters( _param: string) {
    return this.http.get(`${environment.apiURL}/api/parameter/${_param}`);
  };

}
