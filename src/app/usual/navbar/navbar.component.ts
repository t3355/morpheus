import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

// Services
import { UsersService } from '../../auth/services/users.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit{

  userName: string = "";

  constructor( private userService: UsersService,
                private router: Router ) { }

  ngOnInit(): void {
    this.userName = sessionStorage.getItem("userName") || "";
  }

  logoutUser() {
    Swal.fire({
      allowOutsideClick: false,
      icon: 'question',
      title: 'Salir',
      text: 'Salir de la aplicación?',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    })
    .then( (result) => {
      if (result.isConfirmed) {
        this.userService.logoutUser()
            .then( () => {
              // sessionStorage.removeItem('tokenId');
              sessionStorage.removeItem('userUID');
              sessionStorage.removeItem('userName');
              sessionStorage.removeItem('transaccion');
              sessionStorage.removeItem('Banks');
              sessionStorage.removeItem('AmountMax');
              sessionStorage.removeItem('exchange');
              this.router.navigateByUrl('auth/login');
            })
            .catch( error => {
              console.log( 'Error : ', error );
            });
      };
    });
  };

}
