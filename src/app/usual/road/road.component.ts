import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-road',
  templateUrl: './road.component.html',
  styleUrls: ['./road.component.css']
})
export class RoadComponent {

  @Input() road_map: number[] = []
  @Input() road_step: number = 0;

  constructor() { }

}
