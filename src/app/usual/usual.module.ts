import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";

// Components
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RoadComponent } from './road/road.component';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    RoadComponent
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    RoadComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class UsualModule { }
