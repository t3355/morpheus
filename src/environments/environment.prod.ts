export const environment = {
  firebase: {
    projectId: 'morpheus-19cd4',
    appId: '1:125739479875:web:72b4c8c29a73b4126575f6',
    databaseURL: 'https://morpheus-19cd4.firebaseio.com',
    storageBucket: 'morpheus-19cd4.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyASlWvSAHh_ClC9iYDVFQvBpcik9IzMO8A',
    authDomain: 'morpheus-19cd4.firebaseapp.com',
    messagingSenderId: '125739479875',
  },
  production: true,
  useEmulators: false,
  apiURL: 'https://us-central1-morpheus-19cd4.cloudfunctions.net',
  apiAppWeb : 'https://morpheus-19cd4.firebaseapp.com/'
};
