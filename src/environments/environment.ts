// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'morpheus-19cd4',
    appId: '1:125739479875:web:72b4c8c29a73b4126575f6',
    databaseURL: 'https://morpheus-19cd4.firebaseio.com',
    storageBucket: 'morpheus-19cd4.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyASlWvSAHh_ClC9iYDVFQvBpcik9IzMO8A',
    authDomain: 'morpheus-19cd4.firebaseapp.com',
    messagingSenderId: '125739479875',
  },
  production: false,
  useEmulators: true,
  apiURL: 'http://localhost:5001/morpheus-19cd4/us-central1',
  // apiURL: 'https://us-central1-morpheus-19cd4.cloudfunctions.net',
  apiAppWeb : 'http://localhost:4200/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
